/**
 * Copyright (c) 2020 Kota Suizu
 * Released under the MIT license
 * http://opensource.org/licenses/mit-license.php
 **/
'use strict';

const JSON5 = require('json5');

// util class
module.exports = class Utils {
  /**
   * Get Unixtime
   * @param {*} date
   * @return {integer} Unixtime (Miliseconds)
   */
  static getUnixTime(date) {
    if (!date) date = new Date();
    return date.getTime();
  }

  /**
   * Get Datetime
   * @param {date} date
   * @param {integer} offset
   * @return {datetime} YYYY-MM-DD hh:mm:ss.SSS
   */
  static getDateTime(date, offset) {
    if (!date) date = new Date();
    if (!offset) {
      date.setHours(date.getHours());
    } else {
      date.setHours(date.getHours() + offset);
    }
    var d = date.getFullYear() + '-';
    d += ('00' + (date.getMonth() + 1)).slice(-2) + '-';
    d += ('00' + date.getDate()).slice(-2) + ' ';
    d += ('00' + date.getHours()).slice(-2) + ':';
    d += ('00' + date.getMinutes()).slice(-2) + ':';
    d += ('00' + date.getSeconds()).slice(-2);
    return d;
  }

  /**
   * Convert JSON object to flat
   * @param {object} obj
   * @param {string} prefix
   * @return {object} flat json object
   */
  static convertObj(obj, prefix) {
    var results = [];
    for (var key1 in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key1)) {
        if (prefix !== '' && prefix.slice(-1) !== '.') {
          prefix += '.';
        }
        if (typeof obj[key1] === 'object') {
          Array.prototype.push.apply(results, convertObj(obj[key1], prefix + key1));
        } else {
          var resultObj = {
            key: prefix + key1,
            value: obj[key1],
          };
          results.push(resultObj);
        }
      }
    }
    return results;
  }

  /**
   * Type Check
   * @param {string} type
   * @param {object} obj
   * @return {boolean} boolean
   */
  static isTypeOf(type, obj) {
    var clas = Object.prototype.toString.call(obj).slice(8, -1);
    return obj !== undefined && obj !== null && clas === type;
  }

  /**
   * JSON Check
   * @param {string} str
   * @return {boolean} boolean
   */
  static isJson(str) {
    try {
      JSON5.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }
};
