/**
 * Copyright (c) 2020 Kota Suizu
 * Released under the MIT license
 * http://opensource.org/licenses/mit-license.php
 **/
const winston = require('winston');
// Imports the Google Cloud client library for Winston
const { LoggingWinston } = require('@google-cloud/logging-winston');
const loggingWinston = new LoggingWinston();

// Create a Winston logger that streams to Stackdriver Logging
// Logs will be written to: "projects/YOUR_PROJECT_ID/logs/winston_log"
const logger = winston.createLogger({
  level: process.env.NODE_RED_LOG_LEVEL || 'debug',
  format: winston.format.json(),
  defaultMeta: { LOG_LABEL: 'Node-RED-Starter' },
  transports: [
    // Add Stackdriver Logging
    loggingWinston,
  ],
});

module.exports = logger;
