/**
 * Copyright (c) 2020 Kota Suizu
 * Released under the MIT license
 * http://opensource.org/licenses/mit-license.php
 **/
'use strict';

const logger = require('./logger');
const Utils = require('./Utils');
const JSON5 = require('json5');

const levels = {
  off: 1,
  fatal: 10,
  error: 20,
  warn: 30,
  info: 40,
  debug: 50,
  trace: 60,
  audit: 98,
  metric: 99,
};
const levelNames = {
  10: 'fatal',
  20: 'error',
  30: 'warn',
  40: 'info',
  50: 'debug',
  60: 'trace',
  98: 'audit',
  99: 'metric',
};

// util class
module.exports = class logHelper {
  /**
   * log
   * @param {*} msg
   */
  static log(msg) {
    var metadata;
    var message;
    if (msg.level === levels.metric) {
      message = JSON.stringify(msg);
      metadata = {
        LOG_LABEL: 'Node-RED-Metric',
        NODE_RED: {
          msg: msg,
        },
      };
      logger.info(message, metadata);
    } else if (msg.level === levels.audit) {
      message = JSON.stringify(msg);
      metadata = {
        LOG_LABEL: 'Node-RED-Audit',
        NODE_RED: {
          msg: msg,
        },
      };
      logger.info(message, metadata);
    } else {
      var sev = levelNames[msg.level];
      var label = 'Node-RED-Common';
      message = msg.msg;
      var msgObj;
      var stack;
      if (Utils.isJson(msg.msg)) {
        msgObj = JSON5.parse(msg.msg);
        if (Utils.isTypeOf('String', msgObj.LOG_SEVERITY)) {
          sev = msgObj.LOG_SEVERITY;
          delete msgObj.LOG_SEVERITY;
        }
        if (Utils.isTypeOf('String', msgObj.LOG_MESSAGE)) {
          message = msgObj.LOG_MESSAGE;
          delete msgObj.LOG_MESSAGE;
        } else {
          message = 'No message; check the JSON in [jsonPayload.metadata.NODE_RED.msg].';
        }
      } else if (Utils.isTypeOf('String', msg.msg)) {
        msgObj = msg.msg;
        var tempSev = msg.msg.match(/LOG_SEVERITY.*'/);
        if (Utils.isTypeOf('Array', tempSev)) {
          sev = tempSev[0].slice(15, -1);
        }
        var tempMsg = msg.msg.match(/LOG_MESSAGE.*'/);
        if (Utils.isTypeOf('Array', tempSev)) {
          message = tempMsg[0].slice(14, -1);
        }
      }
      if (msg.msg && msg.msg.stack) {
        if (Utils.isJson(msg.msg.stack)) {
          stack = JSON5.parse(msg.msg.stack);
        } else {
          stack = msg.msg.stack;
        }
      }
      if (msg.type) {
        label = 'Node-RED-Flow';
      }
      metadata = {
        LOG_LABEL: label,
        NODE_RED: {
          msg: msgObj,
          stack: stack,
          nodeType: msg.type,
          nodeId: msg.id,
          flowId: msg.z,
          flowPath: msg.path,
        },
      };

      if (sev === 'fatal') {
        logger.crit(message, metadata);
      } else if (sev === 'error') {
        logger.error(message, metadata);
      } else if (sev === 'warn') {
        logger.warn(message, metadata);
      } else if (sev === 'info') {
        logger.info(message, metadata);
      } else {
        logger.debug(message, metadata);
      }
    }
  }
};
