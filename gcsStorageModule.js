/**
 * Copyright (c) 2020 Kota Suizu
 * Released under the MIT license
 * http://opensource.org/licenses/mit-license.php
 **/

const { Storage } = require('@google-cloud/storage');
var when = require('when');
var fs = require('fs');

const logger = require('./utils/logger');

var settings = {};

var storage;
var bucketName;
var bucket;
var defaultsDir = '';
var appname;

function prepopulateFlows(resolve) {
  const myFileName = appname + '/flow.json';
  logger.debug('[gcsStorageModule] prepopulateFlows myFileName is: ' + myFileName);
  const file = bucket.file(myFileName);
  logger.info('[gcsStorageModule] Prepop flows...');
  file.download(function (err, contents) {
    if (err) {
      logger.warn('[gcsStorageModule] Prepop flows download error');
      var promises = [];
      if (fs.existsSync(__dirname + defaultsDir + '/flow.json')) {
        try {
          var flow = fs.readFileSync(__dirname + defaultsDir + '/flow.json', 'utf8');
          var flows = JSON.parse(flow);
          logger.info('[gcsStorageModule] Installing default flow');
          promises.push(gcsStorageModule.saveFlows(flows));
        } catch (err2) {
          logger.error('[gcsStorageModule] Failed to save default flow', err2);
        }
      } else {
        logger.error('[gcsStorageModule] No default flow found');
      }
      if (fs.existsSync(__dirname + defaultsDir + '/flow_cred.json')) {
        try {
          var cred = fs.readFileSync(__dirname + defaultsDir + '/flow_cred.json', 'utf8');
          var creds = JSON.parse(cred);
          logger.info('[gcsStorageModule] Installing default credentials');
          promises.push(gcsStorageModule.saveCredentials(creds));
        } catch (err2) {
          logger.error('[gcsStorageModule] Failed to save default credentials', err2);
        }
      } else {
        logger.error('[gcsStorageModule] No default credentials found');
      }
      Promise.all(promises).then(function () {
        logger.info('[gcsStorageModule] Prepop flows installing done');
        resolve();
      });
    } else {
      // Flows already exist - leave them alone
      logger.info('[gcsStorageModule] Prepop flows already exist');
      resolve();
    }
  });
}

var gcsStorageModule = {
  init: function (_settings) {
    settings.gcsAccount = _settings.gcsAccount || {};
    if (!settings.gcsAccount) {
      logger.error('[gcsStorageModule] GCP Service account is not found');
      var err = Promise.reject('gcsStorageModule service account is not found');
      err.catch((err) => {});
      return err;
    }
    appname = _settings.gaeVersion;
    bucketName = _settings.gcsBucket;

    storage = new Storage({
      projectId: settings.gcsAccount.project_id,
      credentials: settings.gcsAccount,
    });
    bucket = storage.bucket(bucketName);

    return new Promise(function (resolve, reject) {
      bucket.get(function (err, bucket, apiResponse) {
        // `bucket.metadata` has been populated.
        if (err) {
          logger.warn('[gcsStorageModule] GCS get bucket objects error: ' + bucketName);
          var config = {
            location: _settings.gcsLocation,
            storageClass: 'STANDARD',
            iamConfiguration: {
              bucketPolicyOnly: {
                enabled: true,
              },
              uniformBucketLevelAccess: {
                enabled: true,
              },
            },
          };
          storage.createBucket(bucketName, config, function (err) {
            if (err) {
              logger.error('[gcsStorageModule] GCS create bucket error: ' + bucketName);
              reject('Failed to create bucket: ' + err);
            } else {
              logger.info('[gcsStorageModule] GCS create bucket done: ' + bucketName);
              bucket = storage.bucket(bucketName);
              prepopulateFlows(resolve);
            }
          });
        } else {
          prepopulateFlows(resolve);
          resolve();
        }
      });
    });
  },

  getFlows: function () {
    return this.getArrayData('flow');
  },

  saveFlows: function (flows) {
    return this.saveData('flow', flows);
  },

  getCredentials: function () {
    return this.getData('credential');
  },

  saveCredentials: function (creds) {
    return this.saveData('credential', creds);
  },

  getSettings: function () {
    return this.getData('settings');
  },

  saveSettings: function (creds) {
    return this.saveData('settings', creds);
  },

  getData: function (entryType) {
    return when.promise(function (resolve, reject) {
      const myFileName = appname + '/' + entryType + '.json';
      logger.debug('[gcsStorageModule] getData myFileName is: ' + myFileName);
      const file = bucket.file(myFileName);

      file.download(function (err, contents) {
        if (err) {
          if (entryType === 'settings' && err.code === 404) {
            logger.warn('[gcsStorageModule] Failed to get settings. Return the default settings object');
            var dataEntry = {};
            resolve(dataEntry);
          } else {
            logger.error('[gcsStorageModule] GCS file download error: ' + err.message, err);
            reject('Failed to download file: ' + err.message);
          }
        } else {
          var strObj = contents.toString();
          var dataEntry = JSON.parse(strObj);
          resolve(dataEntry);
        }
      });
    });
  },

  getArrayData: function (entryType) {
    return when.promise(function (resolve, reject) {
      const myFileName = appname + '/' + entryType + '.json';
      logger.debug('[gcsStorageModule] getArrayData myFileName is: ' + myFileName);
      const file = bucket.file(myFileName);

      file.download(function (err, contents) {
        if (err) {
          logger.error('[gcsStorageModule] GCS file array download error: ' + err.message, err);
          reject('Failed to download file array: ' + err.message);
        } else {
          var strObj = contents.toString();
          var dataEntry = JSON.parse(strObj);
          resolve(dataEntry);
        }
      });
    });
  },

  saveData: function (entryType, dataEntry) {
    logger.debug('[gcsStorageModule] save data: ' + entryType);
    return when.promise(function (resolve, reject) {
      const myFileName = appname + '/' + entryType + '.json';
      logger.debug('[gcsStorageModule] saveData myFileName is: ' + myFileName);
      const file = bucket.file(myFileName);
      const payload = JSON.stringify(dataEntry);

      file.save(payload, function (err) {
        if (err) {
          logger.error('[gcsStorageModule] GCS file save error: ' + err.message, err);
          reject('Failed to save file: ' + err.message);
        } else {
          resolve();
        }
      });
    });
  },

  // TODO this function needs to be implemented.
  saveLibraryEntry: function (type, path, meta, body) {
    logger.debug('[gcsStorageModule] save library entry: ' + type + ':' + path);
    // if (path.substr(0) != "/") {
    //   path = "/" + path;
    // }
    // return when.promise(function (resolve, reject) {
    //   const myFileName = appname + "/lib/" + type + path;
    //   logger.debug("[gcsStorageModule] saveLibraryEntry myFileName is: " + myFileName);
    //   const file = bucket.file(myFileName);
    //   const jsonPayload = {
    //     meta: meta,
    //     body: body
    //   };
    //   const payload = JSON.stringify(jsonPayload);
    //   file.save(payload, function (err) {
    //     if (err) {
    //       logger.error("[gcsStorageModule] GCS saveLibraryEntry error: " + err.message, err);
    //       reject("Failed to saveLibraryEntry: " + err.message);
    //     } else {
    //       resolve();
    //     }
    //   });
    // });
  },

  // TODO this function needs to be implemented.
  getLibraryEntry: function (type, path) {
    logger.debug('[gcsStorageModule] get library entry: ' + type + ':' + path);
  },
};

module.exports = gcsStorageModule;
