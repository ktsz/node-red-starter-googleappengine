/**
 * Copyright (c) 2019-2021 Kota Suizu
 * Released under the MIT license
 * http://opensource.org/licenses/mit-license.php
 **/
var path = require('path');

const logger = require('./utils/logger');
const logHelper = require('./utils/logHelper');

var userDir = __dirname;

var settings = (module.exports = {
  /*******************************************************************************
   * Flow File and User Directory Settings
   *  - flowFile
   *  - credentialSecret
   *  - flowFilePretty
   *  - userDir
   *  - nodesDir
   ******************************************************************************/
  flowFile: 'flow.json',
  credentialSecret: process.env.NODE_RED_CREDENTIAL_SECRET,
  flowFilePretty: true,
  userDir: userDir,
  nodesDir: path.join(__dirname, 'nodes'),

  /*******************************************************************************
   * Server Settings
   *  - uiPort
   *  - uiHost
   *  - apiMaxLength
   *    - httpServerOptions
   *  - httpAdminRoot
   *    - httpAdminMiddleware
   *    - httpNodeRoot
   *    - httpNodeCors
   *    - httpNodeMiddleware
   *  - httpStatic
   ******************************************************************************/

  uiPort: process.env.PORT || 1880,
  //uiHost: "127.0.0.1",
  apiMaxLength: '5mb',
  httpAdminRoot: '/red',
  httpStatic: path.join(__dirname, 'public'),

  /*******************************************************************************
   * Runtime Settings
   *    - lang
   *  - logging
   *    - contextStorage
   *    - exportGlobalContextKeys
   *  - externalModules
   ******************************************************************************/
  // Configure the logging output
  logging: {
    // Only console logging is currently supported
    console: {
      // Level of logging to be recorded. Options are:
      // fatal - only those errors which make the application unusable should be recorded
      // error - record errors which are deemed fatal for a particular request + fatal errors
      // warn - record problems which are non fatal + errors + fatal errors
      // info - record information about the general running of the application + warn + error + fatal errors
      // debug - record information which is more verbose than info + info + warn + error + fatal errors
      // trace - record very detailed logging + debug + info + warn + error + fatal errors
      // off - turn off all logging (doesn't affect metrics or audit)
      level: process.env.NODE_RED_LOG_LEVEL || 'debug',
      // Whether or not to include metric events in the log output
      metrics: false,
      // Whether or not to include audit events in the log output
      audit: true,
      // handler: logHelper.log(msg)
      handler: function () {
        return function (msg) {
          logHelper.log(msg);
        };
      },
    },
  },
  /** Configure how the runtime will handle external npm modules.
   * This covers:
   *  - whether the editor will allow new node modules to be installed
   *  - whether nodes, such as the Function node are allowed to have their
   * own dynamically configured dependencies.
   * The allow/denyList options can be used to limit what modules the runtime
   * will install/load. It can use '*' as a wildcard that matches anything.
   */
  externalModules: {
    autoInstall: false /** Whether the runtime will attempt to automatically install missing modules */,
    // autoInstallRetry: 30, /** Interval, in seconds, between reinstall attempts */
    // palette: {              /** Configuration for the Palette Manager */
    //     allowInstall: true, /** Enable the Palette Manager in the editor */
    //     allowUpload: true,  /** Allow module tgz files to be uploaded and installed */
    //     allowList: [],
    //     denyList: []
    // },
    // modules: {              /** Configuration for node-specified modules */
    //     allowInstall: true,
    //     allowList: [],
    //     denyList: []
    // }
  },

  /*******************************************************************************
   * Editor Settings
   *  - disableEditor
   *  - nrlint
   *  - editorTheme
   ******************************************************************************/
  disableEditor: true,

  // Add a `nrlint` entry pointing to your nrlint config file
  nrlint: require('./.nrlintrc.js'),

  /** Customising the editor
   * See https://nodered.org/docs/user-guide/runtime/configuration#editor-themes
   * for all available options.
   */
  editorTheme: {
    /** The following property can be used to set a custom theme for the editor.
     * See https://github.com/node-red-contrib-themes/theme-collection for
     * a collection of themes to chose from.
     */
    //theme: "",
    palette: {
      /** The following property can be used to order the categories in the editor
       * palette. If a node's category is not in the list, the category will get
       * added to the end of the palette.
       * If not set, the following default order is used:
       */
      //categories: ['subflows', 'common', 'function', 'network', 'sequence', 'parser', 'storage'],
    },
    projects: {
      /** To enable the Projects feature, set this value to true */
      enabled: false,
      workflow: {
        /** Set the default projects workflow mode.
         *  - manual - you must manually commit changes
         *  - auto - changes are automatically committed
         * This can be overridden per-user from the 'Git config'
         * section of 'User Settings' within the editor
         */
        mode: 'manual',
      },
    },
    codeEditor: {
      /** Select the text editor component used by the editor.
       * Defaults to "ace", but can be set to "ace" or "monaco"
       */
      lib: 'monaco',
      options: {
        /** The follow options only apply if the editor is set to "monaco"
         *
         * theme - must match the file name of a theme in
         * packages/node_modules/@node-red/editor-client/src/vendor/monaco/dist/theme
         * e.g. "tomorrow-night", "upstream-sunburst", "github", "my-theme"
         */
        theme: 'monokai',
        /** other overrides can be set e.g. fontSize, fontFamily, fontLigatures etc.
         * for the full list, see https://microsoft.github.io/monaco-editor/api/interfaces/monaco.editor.istandaloneeditorconstructionoptions.html
         */
        //fontSize: 14,
        //fontFamily: "Cascadia Code, Fira Code, Consolas, 'Courier New', monospace",
        //fontLigatures: true,
      },
    },
  },

  /*******************************************************************************
   * Node Settings
   *    - fileWorkingDirectory
   *  - functionGlobalContext
   *    - functionExternalModules
   *  - nodeMessageBufferMaxLength
   *  - ui (for use with Node-RED Dashboard)
   *  - debugUseColors
   *  - debugMaxLength
   *  - execMaxBufferSize
   *  - httpRequestTimeout
   *  - mqttReconnectTime
   *  - serialReconnectTime
   *  - socketReconnectTime
   *  - socketTimeout
   *  - tcpMsgQueueSize
   *  - inboundWebSocketTimeout
   *  - tlsConfigDisableLocalFiles
   *  - webSocketNodeVerifyClient
   ******************************************************************************/
  /** The following property can be used to set predefined values in Global Context.
   * This allows extra node modules to be made available with in Function node.
   * For example, the following:
   *    functionGlobalContext: { os:require('os') }
   * will allow the `os` module to be accessed in a Function node using:
   *    global.get("os")
   */
  functionGlobalContext: {
    PROCESS_ENV: {
      GAE_APPLICATION: process.env.GAE_APPLICATION,
      GAE_DEPLOYMENT_ID: process.env.GAE_DEPLOYMENT_ID,
      GAE_ENV: process.env.GAE_ENV,
      GAE_INSTANCE: process.env.GAE_INSTANCE,
      GAE_MEMORY_MB: process.env.GAE_MEMORY_MB,
      GAE_RUNTIME: process.env.GAE_RUNTIME,
      GAE_SERVICE: process.env.GAE_SERVICE,
      GAE_VERSION: process.env.GAE_VERSION,
      NODE_ENV: process.env.NODE_ENV,
    },
    json5: require('json5'),
    datefns: require('date-fns'),
    datefnstz: require('date-fns-tz'),
    uuid: require('uuid'),
    lodash: require('lodash'),
    validator: require('validator'),
    deepmerge: require('deepmerge'),
    sqlstring: require('sqlstring'),
  },

  /** The maximum number of messages nodes will buffer internally as part of their
   * operation. This applies across a range of nodes that operate on message sequences.
   * defaults to no limit. A value of 0 also means no limit is applied.
   */
  //nodeMessageBufferMaxLength: 0,

  /** If you installed the optional node-red-dashboard you can set it's path
   * relative to httpNodeRoot
   * Other optional properties include
   *  readOnly:{boolean},
   *  middleware:{function or array}, (req,res,next) - http middleware
   *  ioMiddleware:{function or array}, (socket,next) - socket.io middleware
   */
  //ui: { path: "ui" },

  /** Colourise the console output of the debug node */
  debugUseColors: true,

  /** The maximum length, in characters, of any message sent to the debug sidebar tab */
  debugMaxLength: 1000,

  /** Timeout in milliseconds for HTTP request connections. Defaults to 120s */
  //httpRequestTimeout: 120000,

  /** Retry time in milliseconds for MQTT connections */
  mqttReconnectTime: 15000,

  /** Retry time in milliseconds for TCP socket connections */
  //socketReconnectTime: 10000,

  /** Timeout in milliseconds for TCP server socket connections. Defaults to no timeout */
  //socketTimeout: 120000,

  /** Maximum number of messages to wait in queue while attempting to connect to TCP socket
   * defaults to 1000
   */
  //tcpMsgQueueSize: 2000,

  /** Timeout in milliseconds for inbound WebSocket connections that do not
   * match any configured node. Defaults to 5000
   */
  //inboundWebSocketTimeout: 5000,

  /** To disable the option for using local files for storing keys and
   * certificates in the TLS configuration node, set this to true.
   */
  //tlsConfigDisableLocalFiles: true,

  /** The following property can be used to verify websocket connection attempts.
   * This allows, for example, the HTTP request headers to be checked to ensure
   * they include valid authentication information.
   */
  //webSocketNodeVerifyClient: function(info) {
  //    /** 'info' has three properties:
  //    *   - origin : the value in the Origin header
  //    *   - req : the HTTP request
  //    *   - secure : true if req.connection.authorized or req.connection.encrypted is set
  //    *
  //    * The function should return true if the connection should be accepted, false otherwise.
  //    *
  //    * Alternatively, if this function is defined to accept a second argument, callback,
  //    * it can be used to verify the client asynchronously.
  //    * The callback takes three arguments:
  //    *   - result : boolean, whether to accept the connection or not
  //    *   - code : if result is false, the HTTP error status to return
  //    *   - reason: if result is false, the HTTP reason string to return
  //    */
  //},

  //Flag for enabling Appmetrics dashboard (https://github.com/RuntimeTools/appmetrics-dash)
  useAppmetrics: false,
  useStorageModule: false,
  // Blacklist the non-bluemix friendly nodes
  nodesExcludes: ['90-exec.js', '28-tail.js', '10-file.js', '23-watch.js'],
});

// Identify the GCS instance the application should be using.
if (!process.env.NODE_RED_GCS_ACCOUNT) {
  // No suitable service has been found. Fall back to localfilesystem storage
  logger.warn('Failed to get NODE_RED_GCS_ACCOUNT. Falling back to localfilesystem storage. Changes will *not* be saved across application restarts.');
} else {
  // Set the GCS module settings
  const replacedStr = process.env.NODE_RED_GCS_ACCOUNT.replace(/@/g, ' ');
  settings.gcsAccount = JSON.parse(Buffer.from(replacedStr, 'base64').toString());
  if (!process.env.NODE_RED_GCS_BUCKET) {
    settings.gcsBucket = process.env.GOOGLE_CLOUD_PROJECT + '-gae-' + process.env.GAE_SERVICE + '-node-red';
  } else {
    settings.gcsBucket = process.env.NODE_RED_GCS_BUCKET;
  }
  settings.gcsLocation = process.env.NODE_RED_GCS_LOCATION || 'ASIA-NORTHEAST1';
  settings.gaeVersion = process.env.GAE_VERSION;

  settings.storageModule = require('./gcsStorageModule');
  settings.useStorageModule = true;
  logger.info('Using GCS Storage Module.');
}
