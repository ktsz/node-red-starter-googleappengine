Node-RED-Starter-GoogleAppEngine
=========================

Node-RED Starter Blank Project for Google App Engine Flexible/Standard Environment


# Ready to use
## Google Cloud Platform
Node-RED-Starter-GoogleAppEngine uses GCP's App Engine Flexible or Standard environment as its runtime environment. For more information on GAE, please check the following site.  
https://cloud.google.com/appengine?hl=ja

### Enable APIs
The following 5 APIs are required.  
Open the hamburger menu in the upper left > "APIs and Services" > "Dashboard" > "Enable APIs and Services" and enable it.  
- Compute Engine API
- App Engine Admin API
- Google App Engine Flexible Environment
- Cloud Build API
- Cloud Deployment Manager V2 API

### Create a service account
Create a service account to be used during deployment and a service account to be used with the Node-RED storageModule.  
Create a service account from the hamburger menu in the upper left > "IAM and management" > "Service accounts" > "Create service account".  
- for gitlab-ci deploy
  - Name, etc.
    - Service account name: in this case, "gitlab-ci"
    - Service account ID: in this case, "gitlab-ci"
  - Authority
    - App Engine Service Administrator
    - App Engine deployer
    - Cloud Build Editor
    - Storage object administrator
    - Service Account User
- for Node-RED storageModule
  - Name, etc.
    - Service account name: in this case, "node-red-gcs"
    - Service account ID: in this case, "node-red-gcs"
  - Authority
    - Storage administrator
    - Service Account User

Finally, create the key in JSON format and download it. (Please keep it safe.)

### Enable GAE
Enable GAE. Please refer to the following settings to enable GAE.  

| Key | Value |
| ------ | ------ |
| Region | in this case, "asia-northeast1 (Tokyo)" |
| Language | Node.js |
| Environment | Flexible |


## Preparing for Gitlab
Prepare Node-RED starter code for GAE and configure CI/CD for automatic deployment to GAE.  

### Preparing the Starter Code
Import your project. You can find the starter code below and import this into your project.  
https://gitlab.com/ktsz/node-red-starter-googleappengine.git

### CI/CD Configuration
Set up CI/CD variables.  
From your Gitlab project, go to Settings > CI/CD in the left menu, expand the Variables section and set the following variables

| Key | value |
| ------ | ------ |
| GCP_PROJECT_ID | GCP Project ID |
| GCP_SERVICE_ACCOUNT | The service account to use when deploying to GAE. Copy the text in the service account key JSON file and set it |
| NODE_RED_CREDENTIAL_SECRET | Node-RED flow encryption key (arbitrary value) |
| NODE_RED_USE_EDITOR | Node-RED editor availability (default is false) |
| NODE_RED_USE_APPMETRICS | AppMetrics availability (default is false) |
| NODE_RED_USERNAME | Node-RED editor login user ID |
| NODE_RED_PASSWORD | Node-RED editor login user PW |
| NODE_RED_GCS_ACCOUNT | The GCS service account for use with Node-RED storageModule. Copy the text in the service account key JSON file and set it (If unset, use localStorage) |
| NODE_RED_GCS_LOCATION | Node-RED log level (default is ASIA-NORTHEAST1) |
| NODE_RED_LOG_LEVEL | Node-RED log level (default is info) |


## Preparation of the Node-RED development environment
### Install to local environment
Please install it with the following command.  
```
sudo npm install -g --unsafe-perm node-red
```
Reference: https://nodered.jp/docs/getting-started/local  

### Enable Node-RED project feature
There is a section on project features at the bottom of the <installation> /settings.js file. The default is false, so change it to true to enable the project feature.  
```
// Customising the editor
editorTheme: {
    projects: {
        // To enable the Projects feature, set this value to true
       enabled: true
    }
}
```

### Launch Node-RED
Start with the following command.  
```
node-red
```

### Clone project
Access your local Node-RED in your browser, go to the hamburger menu in the upper right corner of the screen, click "Clone Project", enter the following information and click the "Clone Project" button.  

| Key | Value |
| ------ | ------ |
| Project name | set your project name |
| URL of Git repository | the URL of the Gitlab repository you just imported and prepared |
| Username | Gitlab login user ID |
| Password | Gitlab login user PW |

### Flow encryption settings
Configure the flow encryption settings to encrypt security information such as API tokens that are set for the node. Open the hamburger menu in the upper right corner > Project > Settings > Settings on the left tab. In the upper right corner of the window, there is an "Edit" button.

| Key | Value |
| ------ | ------ |
| New encryption key | the same value as the flow encryption key set in the CI/CD variable in Gitlab |

## Copyright and license

Copyright (c) 2019-2021 Kota Suizu  
Released under the MIT license  
http://opensource.org/licenses/mit-license.php
