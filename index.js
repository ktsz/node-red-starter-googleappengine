/**
 * Copyright (c) 2019 Kota Suizu
 * Released under the MIT license
 * http://opensource.org/licenses/mit-license.php
 **/
var when = require('when');
var bcrypt = require('bcrypt');

const logger = require('./utils/logger');

logger.info();
logger.info('Node-RED-Starter Starting on Google Cloud AppEngine');
logger.info('===================================================');
logger.info();
const processEnv = {
  GAE_APPLICATION: process.env.GAE_APPLICATION,
  GAE_DEPLOYMENT_ID: process.env.GAE_DEPLOYMENT_ID,
  GAE_ENV: process.env.GAE_ENV,
  GAE_INSTANCE: process.env.GAE_INSTANCE,
  GAE_MEMORY_MB: process.env.GAE_MEMORY_MB,
  GAE_RUNTIME: process.env.GAE_RUNTIME,
  GAE_SERVICE: process.env.GAE_SERVICE,
  GAE_VERSION: process.env.GAE_VERSION,
  NODE_ENV: process.env.NODE_ENV,
  NODE_RED_USE_EDITOR: process.env.NODE_RED_USE_EDITOR,
  NODE_RED_USE_APPMETRICS: process.env.NODE_RED_USE_APPMETRICS,
  NODE_RED_GCS_LOCATION: process.env.NODE_RED_GCS_LOCATION,
  NODE_RED_LOG_LEVEL: process.env.NODE_RED_LOG_LEVEL,
};

logger.info('Environment variables (see in the metadata): ', processEnv);
logger.debug('process.env (see in the metadata): ', process.env);
logger.info('Loading settings.js ...');
var settings = require('./settings.js');

if (!settings.adminAuth && process.env.NODE_RED_USERNAME && process.env.NODE_RED_PASSWORD) {
  // No user-defined security
  logger.info('Enabling adminAuth using NODE_RED_USERNAME/NODE_RED_PASSWORD');
  var config = {
    adminAuth: {
      username: process.env.NODE_RED_USERNAME,
      password: bcrypt.hashSync(process.env.NODE_RED_PASSWORD, 8),
      allowAnonymous: process.env.NODE_RED_GUEST_ACCESS === 'true',
    },
  };
  startNodeRED(config);
} else {
  logger.info('Enabling adminAuth using settings.adminAuth');
  startNodeRED({});
}

function startNodeRED(config) {
  if (config.adminAuth && !settings.adminAuth) {
    logger.info('Enabling adminAuth security - set NODE_RED_USERNAME and NODE_RED_PASSWORD to change credentials');
    settings.adminAuth = {
      type: 'credentials',
      users: function (username) {
        if (config.adminAuth.username == username) {
          return when.resolve({
            username: username,
            permissions: '*',
          });
        } else {
          return when.resolve(null);
        }
      },
      authenticate: function (username, password) {
        if (config.adminAuth.username === username && bcrypt.compareSync(password, config.adminAuth.password)) {
          return when.resolve({
            username: username,
            permissions: '*',
          });
        } else {
          return when.resolve(null);
        }
      },
    };
    if (process.env.NODE_RED_GUEST_ACCESS === 'true' || (process.env.NODE_RED_GUEST_ACCESS === undefined && config.adminAuth.allowAnonymous)) {
      logger.info("Enabling anonymous read-only access - set NODE_RED_GUEST_ACCESS to 'false' to disable");
      settings.adminAuth.default = function () {
        return when.resolve({
          anonymous: true,
          permissions: 'read',
        });
      };
    } else {
      logger.info("Disabled anonymous read-only access - set NODE_RED_GUEST_ACCESS to 'true' to enable");
    }
  }

  if (!config.adminAuth && !settings.adminAuth) {
    settings.disableEditor = true;
  } else {
    if (process.env.NODE_RED_USE_EDITOR) {
      if (process.env.NODE_RED_USE_EDITOR.toLowerCase() === 'true') {
        settings.disableEditor = false;
      } else if (process.env.NODE_RED_USE_EDITOR.toLowerCase() === 'false') {
        settings.disableEditor = true;
      }
    }
  }

  var dash;
  // ensure the environment variable overrides the settings
  if (process.env.NODE_RED_USE_APPMETRICS === 'true' || (settings.useAppmetrics && !(process.env.NODE_RED_USE_APPMETRICS === 'false'))) {
    dash = require('appmetrics-dash');
    dash.attach();
  }
  const settingsInfo = {
    userDir: settings.userDir,
    flowFile: settings.flowFile,
    nodesDir: settings.nodesDir,
    disableEditor: settings.disableEditor,
    useAppmetrics: settings.useAppmetrics,
    loggingConsoleLevel: settings.logging.console.level,
    loggingConsoleMetrics: settings.logging.console.metrics,
    loggingConsoleAudit: settings.logging.console.audit,
    gcsBucket: settings.gcsBucket,
    gcsLocation: settings.gcsLocation,
    useStorageModule: settings.useStorageModule,
  };
  logger.info('Settings Info (see in the metadata): ', settingsInfo);

  logger.info('Node-RED-Starter Loading application settings done. Starting Node-RED...');
  logger.info();
  require('./node_modules/node-red/red.js');
}
