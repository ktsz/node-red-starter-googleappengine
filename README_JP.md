Node-RED-Starter-GoogleAppEngine
=========================

Node-RED Starter Blank Project for Google App Engine Flexible/Standard Environment

# 利用準備
## Google Cloud Platform の準備
Node-RED-Starter-GoogleAppEngineは、実行環境としてGCPのApp Engine FlexibleもしくはStandard環境を利用します。GAEの詳細については、以下サイトをご確認ください。  
https://cloud.google.com/appengine?hl=ja

### API有効化
以下5つのAPIが必要となります。  
左上のハンバーガーメニュー >「APIとサービス」>「ダッシュボード」>「APIとサービスを有効化」を開き、有効化ください。  
- Compute Engine API
- App Engine Admin API
- Google App Engine Flexible Environment
- Cloud Build API
- Cloud Deployment Manager V2 API

### サービスアカウント作成
デプロイ時に使用するサービスアカウントと、Node-RED storageModuleで使用するサービスアカウントを作成します。  
左上のハンバーガーメニュー >「IAMと管理」>「サービスアカウント」>「サービスアカウントを作成」より作成ください。  
- gitlab-ci デプロイ用
  - 名称等
    - サービスアカウント名：任意（ここでは gitlab-ci）
    - サービスアカウントID：任意（ここでは gitlab-ci）
  - 権限
    - App Engine サービス管理者
    - App Engine デプロイ担当者
    - Cloud Build 編集者
    - ストレージオブジェクト 管理者
    - サービス アカウント ユーザー
- Node-RED storageModule用
  - 名称等
    - サービスアカウント名：任意（ここでは node-red-gcs）
    - サービスアカウントID：任意（ここでは node-red-gcs）
  - 権限
    - ストレージ 管理者
    - サービス アカウント ユーザー

最後に、JSON形式でキーを作成し、ダウンロードしてください。（安全に保管してください。）

### GAE有効化
GAEを有効化します。以下設定値を参考に有効化ください。  

| 項目 | 値 |
| ------ | ------ |
| Region | 任意（ここでは asia-northeast1（東京）を選択） |
| Language | Node.js |
| Environment | フレキシブル |


## Gitlab の準備
GAE用のNode-REDスターターコードの準備と、GAEに自動デプロイするためのCI/CDの設定を行います。  

### スターターコード準備
プロジェクトをインポートします。以下にスターターコードがありますので、こちらを自分のプロジェクトへインポートください。  
https://gitlab.com/ktsz/node-red-starter-googleappengine.git

### CI/CD設定
CI/CDの変数を設定します。  
Gitlabプロジェクトより、左メニューの設定 > CI/CD を開き、Variablesセクションを展開、以下変数を設定ください。

| キー | 値 |
| ------ | ------ |
| GCP_PROJECT_ID | GCPのプロジェクトID |
| GCP_SERVICE_ACCOUNT | GAEへのデプロイ時に使用するサービスアカウント　キーJSONファイル内本文をコピーし設定 |
| NODE_RED_CREDENTIAL_SECRET | Node-REDフロー暗号化キー（任意の値） |
| NODE_RED_USE_EDITOR | Node-REDのエディタ使用可否（デフォルトはfalse） |
| NODE_RED_USE_APPMETRICS | AppMetricsの使用可否（デフォルトはfalse） |
| NODE_RED_USERNAME | Node-REDのエディタのログインユーザーID |
| NODE_RED_PASSWORD | Node-REDのエディタのログインユーザーPW |
| NODE_RED_GCS_ACCOUNT | Node-RED storageModuleで使用するGCS用サービスアカウント　キーJSONファイル内本文をコピーし設定（未設定の場合はlocalStorage） |
| NODE_RED_GCS_LOCATION | Node-RED storageModuleで使用するGCSのBucketロケーション（デフォルトはASIA-NORTHEAST1） |
| NODE_RED_LOG_LEVEL | Node-REDのログ出力レベル（デフォルトはinfo） |

## Node-RED開発環境の準備
### ローカル環境へインストール
以下コマンドでインストールください。  
```
sudo npm install -g --unsafe-perm node-red
```
参考： https://nodered.jp/docs/getting-started/local  

### Node-REDプロジェクト機能有効化
<インストール先>/settings.jsファイルの一番下にプロジェクト機能のセクションがあります。デフォルトは false になってますので、ここを true に変更し、プロジェクト機能を有効化します。
```
// Customising the editor
editorTheme: {
    projects: {
        // To enable the Projects feature, set this value to true
       enabled: true
    }
}
```

### Node-REDを起動
以下コマンドで起動ください。
```
node-red
```

### プロジェクトをクローン
ブラウザでローカルのNode-REDへアクセス、右上のハンバーガーメニュー > プロジェクト > 新規　を開き、「プロジェクトをクローン」を押下、以下必要情報を入力し、「プロジェクトをクローン」ボタンを押下ください。  

| 項目 | 値 |
| ------ | ------ |
| プロジェクト名 | 任意 |
| GitリポジトリのURL | 先程インポートし準備したGitlabリポジトリのURL |
| ユーザー名 | GitlabのログインユーザーID |
| パスワード | GitlabのログインユーザーPW |

### フロー暗号化設定
ノードに設定するAPIトークン等のセキュリティ情報を暗号化するため、フローの暗号化設定を行います。右上のハンバーガーメニュー > プロジェクト > 設定 > 左タブの設定　を開きます。そのウィンドウの右上に「編集」ボタンがありますので、こちらから設定ください。

| 項目 | 値 |
| ------ | ------ |
| 新規の暗号化キー | GitlabのCI/CD変数にて設定したフロー暗号化キーと同じ値 |


## Copyright and license

Copyright (c) 2019-2021 Kota Suizu  
Released under the MIT license  
http://opensource.org/licenses/mit-license.php
